﻿using System.Collections.Generic;
using System.Windows;

namespace _32._1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            slider.Value = 4;
        }

        //ObservableCollection<Ellips> lijstEllipsen = new ObservableCollection<Ellips>();


        //private void Window_Loaded(object sender, RoutedEventArgs e)
        //{

        //    lijstEllipsen.Add(new Ellips("Red", 50, 20));
        //    lijstEllipsen.Add(new Ellips("Blue", 100, 20));
        //    lijstEllipsen.Add(new Ellips("Green", 50, 40));

        //}

        private void txtHoogte_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            //Ellips ellips = new Ellips("Red", 50, 20);
            //ellips.Hoogte = Convert.ToInt32(txtHoogte.Text);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            List<string> lijstKleuren = new List<string>();
            lijstKleuren.Add("Red");
            lijstKleuren.Add("Blue");
            lijstKleuren.Add("Green");

            cmbKaderKleur.ItemsSource = lijstKleuren;
            cmbVullingKleur.ItemsSource = lijstKleuren;

            //lijstEllipsen.Add(new Ellips("Red", 50, 20));
            //lijstEllipsen.Add(new Ellips("Blue", 100, 20));
            //lijstEllipsen.Add(new Ellips("Green", 50, 40));
        }

        //private void Window_Loaded_1(object sender, RoutedEventArgs e)
        //{

        //}
    }
}
